package main

import (
	"codeberg.org/woodpecker-plugins/plugin-docker-buildx/plugin"
	"github.com/urfave/cli/v3"
)

// settingsFlags has the cli.Flags for the plugin.Settings.
func settingsFlags(settings *plugin.Settings) []cli.Flag {
	flags := []cli.Flag{
		&cli.BoolFlag{
			Name:        "dry-run",
			Sources:     cli.EnvVars("PLUGIN_DRY_RUN"),
			Usage:       "disables docker push",
			Destination: &settings.Dryrun,
		},
		&cli.StringFlag{
			Name:        "remote.url",
			Sources:     cli.EnvVars("CI_REMOTE_URL", "DRONE_REMOTE_URL"),
			Usage:       "sets the git remote url",
			Destination: &settings.Build.Remote,
		},
		&cli.StringFlag{
			Name:        "daemon.mirror",
			Sources:     cli.EnvVars("PLUGIN_MIRROR", "DOCKER_PLUGIN_MIRROR"),
			Usage:       "sets a registry mirror to pull images",
			Destination: &settings.Daemon.Mirror,
		},
		&cli.StringFlag{
			Name:        "daemon.storage-driver",
			Sources:     cli.EnvVars("PLUGIN_STORAGE_DRIVER"),
			Usage:       "sets the docker daemon storage driver",
			Destination: &settings.Daemon.StorageDriver,
		},
		&cli.StringFlag{
			Name:        "daemon.storage-path",
			Sources:     cli.EnvVars("PLUGIN_STORAGE_PATH"),
			Usage:       "sets the docker daemon storage path",
			Value:       "/var/lib/docker",
			Destination: &settings.Daemon.StoragePath,
		},
		&cli.StringFlag{
			Name:        "daemon.bip",
			Sources:     cli.EnvVars("PLUGIN_BIP"),
			Usage:       "allows the docker daemon to bride ip address",
			Destination: &settings.Daemon.Bip,
		},
		&cli.StringFlag{
			Name:        "daemon.mtu",
			Sources:     cli.EnvVars("PLUGIN_MTU"),
			Usage:       "sets docker daemon custom mtu setting",
			Destination: &settings.Daemon.MTU,
		},
		&cli.StringSliceFlag{
			Name:        "daemon.dns",
			Sources:     cli.EnvVars("PLUGIN_CUSTOM_DNS"),
			Usage:       "sets custom docker daemon dns server",
			Destination: &settings.Daemon.DNS,
		},
		&cli.StringSliceFlag{
			Name:        "daemon.dns-search",
			Sources:     cli.EnvVars("PLUGIN_CUSTOM_DNS_SEARCH"),
			Usage:       "sets custom docker daemon dns search domain",
			Destination: &settings.Daemon.DNSSearch,
		},
		&cli.BoolFlag{
			Name:        "daemon.insecure",
			Sources:     cli.EnvVars("PLUGIN_INSECURE"),
			Usage:       "allows the docker daemon to use insecure registries",
			Destination: &settings.Daemon.Insecure,
		},
		&cli.BoolFlag{
			Name:        "daemon.ipv6",
			Sources:     cli.EnvVars("PLUGIN_IPV6"),
			Usage:       "enables docker daemon IPv6 support",
			Destination: &settings.Daemon.IPv6,
		},
		&cli.BoolFlag{
			Name:        "daemon.experimental",
			Sources:     cli.EnvVars("PLUGIN_EXPERIMENTAL"),
			Usage:       "enables docker daemon experimental mode",
			Destination: &settings.Daemon.Experimental,
		},
		&cli.BoolFlag{
			Name:        "daemon.debug",
			Sources:     cli.EnvVars("PLUGIN_DEBUG", "DOCKER_LAUNCH_DEBUG"),
			Usage:       "enables verbose debug mode for the docker daemon",
			Destination: &settings.Daemon.Debug,
		},
		&cli.BoolFlag{
			Name:        "daemon.off",
			Sources:     cli.EnvVars("PLUGIN_DAEMON_OFF"),
			Usage:       "disables the startup of the docker daemon",
			Destination: &settings.Daemon.Disabled,
		},
		&cli.StringFlag{
			Name:        "daemon.ssh-key",
			Sources:     cli.EnvVars("PLUGIN_SSH_KEY", "SSH_KEY"),
			Usage:       "adds a ssh key to be used by the daemon",
			Destination: &settings.Daemon.SshKey,
		},
		&cli.StringSliceFlag{
			Name:        "daemon.remote-builder",
			Sources:     cli.EnvVars("PLUGIN_REMOTE_BUILDERS", "REMOTE_BUILDERS"),
			Usage:       "adds remote builders to be used by buildx",
			Destination: &settings.Daemon.RemoteBuilders,
		},
		&cli.BoolFlag{
			Name:        "daemon.buildkit-debug",
			Sources:     cli.EnvVars("PLUGIN_BUILDKIT_DEBUG"),
			Usage:       "enables buildkit debug",
			Destination: &settings.Daemon.BuildkitDebug,
		},
		&cli.StringFlag{
			Name:        "dockerfile",
			Sources:     cli.EnvVars("PLUGIN_DOCKERFILE"),
			Usage:       "sets dockerfile to use for the image build",
			Value:       "Dockerfile",
			Destination: &settings.Build.Dockerfile,
		},
		&cli.StringFlag{
			Name:        "context",
			Sources:     cli.EnvVars("PLUGIN_CONTEXT"),
			Usage:       "sets the path of the build context to use",
			Value:       ".",
			Destination: &settings.Build.Context,
		},
		&cli.StringSliceFlag{
			Name: "tags",
			Sources: cli.NewValueSourceChain(
				cli.File(".tags"),
				cli.EnvVar("PLUGIN_TAG"),
				cli.EnvVar("PLUGIN_TAGS"),
			),
			Usage:       "sets repository tags to use for the image",
			Value:       []string{"latest"},
			Destination: &settings.Build.Tags,
		},
		&cli.StringFlag{
			Name:        "tags.file",
			Sources:     cli.EnvVars("PLUGIN_TAGS_FILE", "PLUGIN_TAG_FILE"),
			Usage:       "overwrites tags flag with values find in set file",
			Destination: &settings.Build.TagsFile,
		},
		&cli.BoolFlag{
			Name:        "tags.auto",
			Sources:     cli.EnvVars("PLUGIN_AUTO_TAG"),
			Usage:       "generates tag names automatically based on git branch and git tag",
			Destination: &settings.Build.TagsAuto,
		},
		&cli.StringFlag{
			Name:        "tags.defaultName",
			Sources:     cli.EnvVars("PLUGIN_DEFAULT_TAG"),
			Usage:       "allows setting an alternative to `latest` for the auto tag",
			Destination: &settings.Build.TagsDefaultName,
			Value:       "latest",
		},
		&cli.StringFlag{
			Name:        "tags.suffix",
			Sources:     cli.EnvVars("PLUGIN_DEFAULT_SUFFIX", "PLUGIN_AUTO_TAG_SUFFIX"),
			Usage:       "generates tag names with the given suffix",
			Destination: &settings.Build.TagsSuffix,
		},
		&cli.StringSliceFlag{
			Name:        "labels",
			Sources:     cli.EnvVars("PLUGIN_LABEL", "PLUGIN_LABELS"),
			Usage:       "sets labels to use for the image",
			Destination: &settings.Build.Labels,
		},
		&cli.BoolFlag{
			Name:        "labels.auto",
			Sources:     cli.EnvVars("PLUGIN_DEFAULT_LABELS", "PLUGIN_AUTO_LABEL"),
			Usage:       "generates labels automatically based on git repository info",
			Value:       true,
			Destination: &settings.Build.LabelsAuto,
		},
		&cli.StringFlag{
			Name:        "args",
			Sources:     cli.EnvVars("PLUGIN_BUILD_ARGS"),
			Usage:       "sets custom build arguments for the build",
			Destination: &settings.Build.ArgsRaw,
		},
		&cli.StringSliceFlag{
			Name:        "args-from-env",
			Sources:     cli.EnvVars("PLUGIN_BUILD_ARGS_FROM_ENV"),
			Usage:       "forwards environment variables as custom arguments to the build",
			Destination: &settings.Build.ArgsEnv,
		},
		&cli.StringSliceFlag{
			Name:        "secrets",
			Sources:     cli.EnvVars("PLUGIN_SECRETS"),
			Usage:       "sets custom secret arguments for the build",
			Destination: &settings.Build.Secrets,
		},
		&cli.BoolFlag{
			Name:        "quiet",
			Sources:     cli.EnvVars("PLUGIN_QUIET"),
			Usage:       "enables suppression of the build output",
			Destination: &settings.Build.Quiet,
		},
		&cli.StringFlag{
			Name:        "target",
			Sources:     cli.EnvVars("PLUGIN_TARGET"),
			Usage:       "sets the build target to use",
			Destination: &settings.Build.Target,
		},
		&cli.StringFlag{
			Name:        "cache-from",
			Sources:     cli.EnvVars("PLUGIN_CACHE_FROM"),
			Usage:       "sets images to consider as cache sources",
			Destination: &settings.Build.CacheFrom,
		},
		&cli.StringFlag{
			Name:        "cache-to",
			Sources:     cli.EnvVars("PLUGIN_CACHE_TO"),
			Usage:       "cache destination for the build cache",
			Destination: &settings.Build.CacheTo,
		},
		&cli.StringSliceFlag{
			Name:        "cache-images",
			Sources:     cli.EnvVars("PLUGIN_CACHE_IMAGES"),
			Usage:       "list of images to use for build cache. applies both to and from flags for each image",
			Destination: &settings.Build.CacheImages,
		},
		&cli.BoolFlag{
			Name:        "pull-image",
			Sources:     cli.EnvVars("PLUGIN_PULL_IMAGE"),
			Usage:       "enforces to pull base image at build time",
			Value:       true,
			Destination: &settings.Build.Pull,
		},
		&cli.BoolFlag{
			Name:        "compress",
			Sources:     cli.EnvVars("PLUGIN_COMPRESS"),
			Usage:       "enables compression og the build context using gzip",
			Destination: &settings.Build.Compress,
		},
		&cli.StringSliceFlag{
			Name:        "repo",
			Sources:     cli.EnvVars("PLUGIN_REPO"),
			Usage:       "sets repository name for the image",
			Destination: &settings.Build.Repo,
		},
		&cli.StringFlag{
			Name:        "docker.registry",
			Sources:     cli.EnvVars("PLUGIN_REGISTRY", "DOCKER_REGISTRY"),
			Usage:       "sets docker registry to authenticate with",
			Value:       "https://index.docker.io/v1/",
			Destination: &settings.DefaultLogin.Registry,
		},
		&cli.StringFlag{
			Name:        "docker.username",
			Sources:     cli.EnvVars("PLUGIN_USERNAME", "DOCKER_USERNAME"),
			Usage:       "sets username to authenticates with",
			Destination: &settings.DefaultLogin.Username,
		},
		&cli.StringFlag{
			Name:        "docker.password",
			Sources:     cli.EnvVars("PLUGIN_PASSWORD", "DOCKER_PASSWORD"),
			Usage:       "sets password to authenticates with",
			Destination: &settings.DefaultLogin.Password,
		},
		&cli.StringFlag{
			Name:        "docker.email",
			Sources:     cli.EnvVars("PLUGIN_EMAIL", "DOCKER_EMAIL"),
			Usage:       "sets email address to authenticates with",
			Destination: &settings.DefaultLogin.Email,
		},
		&cli.StringFlag{
			Name:        "docker.config",
			Sources:     cli.EnvVars("PLUGIN_CONFIG", "DOCKER_PLUGIN_CONFIG"),
			Usage:       "sets content of the docker daemon json config",
			Destination: &settings.DefaultLogin.Config,
		},
		&cli.StringFlag{
			Name:        "logins",
			Sources:     cli.EnvVars("PLUGIN_LOGINS"),
			Usage:       "list of login",
			Destination: &settings.LoginsRaw,
			Value:       "[]",
		},
		&cli.BoolFlag{
			Name:        "docker.purge",
			Sources:     cli.EnvVars("PLUGIN_PURGE"),
			Usage:       "enables cleanup of the docker environment at the end of a build",
			Value:       true,
			Destination: &settings.Cleanup,
		},
		&cli.BoolFlag{
			Name:        "no-cache",
			Sources:     cli.EnvVars("PLUGIN_NO_CACHE"),
			Usage:       "disables the usage of cached intermediate containers",
			Destination: &settings.Build.NoCache,
		},
		&cli.StringSliceFlag{
			Name:        "add-host",
			Sources:     cli.EnvVars("PLUGIN_ADD_HOST"),
			Usage:       "sets additional host:ip mapping",
			Destination: &settings.Build.AddHost,
		},
		&cli.StringSliceFlag{
			Name:        "platforms",
			Sources:     cli.EnvVars("PLUGIN_PLATFORMS"),
			Usage:       "sets target platform for build",
			Destination: &settings.Build.Platforms,
		},
		&cli.StringFlag{
			Name:        "output",
			Sources:     cli.EnvVars("PLUGIN_OUTPUT"),
			Usage:       "sets build output type and destination configuration",
			Destination: &settings.Build.Output,
		},
		&cli.StringFlag{
			Name:        "ecr.aws_access_key_id",
			Sources:     cli.EnvVars("PLUGIN_AWS_ACCESS_KEY_ID"),
			Usage:       "Access Key ID for AWS",
			Destination: &settings.AwsAccessKeyId,
		},
		&cli.StringFlag{
			Name:        "ecr.aws_secret_access_key_id",
			Sources:     cli.EnvVars("PLUGIN_AWS_SECRET_ACCESS_KEY"),
			Usage:       "Secret Access Key for AWS",
			Destination: &settings.AwsSecretAccessKey,
		},
		&cli.StringFlag{
			Name:        "ecr.aws_region",
			Sources:     cli.EnvVars("PLUGIN_AWS_REGION"),
			Usage:       "AWS region to use",
			Destination: &settings.AwsRegion,
		},
		&cli.BoolFlag{
			Name:        "ecr.create_repository",
			Sources:     cli.EnvVars("PLUGIN_ECR_CREATE_REPOSITORY"),
			Usage:       "creates the ECR repository if it does not exist",
			Destination: &settings.EcrCreateRepository,
		},
		&cli.StringFlag{
			Name:        "ecr.lifecycle_policy",
			Sources:     cli.EnvVars("PLUGIN_ECR_LIFECYCLE_POLICY"),
			Usage:       "AWS ECR lifecycle policy",
			Destination: &settings.EcrLifecyclePolicy,
		},
		&cli.StringFlag{
			Name:        "ecr.repository_policy",
			Sources:     cli.EnvVars("PLUGIN_ECR_REPOSITORY_POLICY"),
			Usage:       "AWS ECR repository policy",
			Destination: &settings.EcrRepositoryPolicy,
		},
		&cli.BoolFlag{
			Name:        "ecr.scan_on_push",
			Sources:     cli.EnvVars("PLUGIN_ECR_SCAN_ON_PUSH"),
			Usage:       "AWS: whether to enable image scanning on push",
			Destination: &settings.EcrScanOnPush,
		},
		&cli.StringFlag{
			Name:        "provenance",
			Sources:     cli.EnvVars("PLUGIN_PROVENANCE"),
			Usage:       "defines provenance setting",
			Destination: &settings.Build.Provenance,
		},
		&cli.StringFlag{
			Name:        "http_proxy",
			Sources:     cli.EnvVars("PLUGIN_HTTP_PROXY", "HTTP_PROXY"),
			Usage:       "sets the HTTP_PROXY env",
			Destination: &settings.ProxyConf.Http,
		},
		&cli.StringFlag{
			Name:        "https_proxy",
			Sources:     cli.EnvVars("PLUGIN_HTTPS_PROXY", "HTTPS_PROXY"),
			Usage:       "sets the HTTPS_PROXY env",
			Destination: &settings.ProxyConf.Https,
		},
		&cli.StringFlag{
			Name:        "no_proxy",
			Sources:     cli.EnvVars("PLUGIN_NO_PROXY", "NO_PROXY"),
			Usage:       "sets the NO_PROXY env",
			Destination: &settings.ProxyConf.No,
		},
		&cli.StringFlag{
			Name:        "sbom",
			Sources:     cli.EnvVars("PLUGIN_SBOM"),
			Usage:       "defines sbom setting",
			Destination: &settings.Build.Sbom,
		},
	}

	if Insecure {
		flags = append(flags,
			&cli.StringFlag{
				Name:        "daemon.buildkit-config",
				Sources:     cli.EnvVars("PLUGIN_BUILDKIT_CONFIG"),
				Usage:       "sets content of the docker buildkit json config",
				Destination: &settings.Daemon.BuildkitConfig,
			},
			&cli.StringSliceFlag{
				Name:        "daemon.buildkit-driveropt",
				Sources:     cli.EnvVars("PLUGIN_BUILDKIT_DRIVEROPT"),
				Usage:       "adds optional driver-ops args like 'env.http_proxy'",
				Destination: &settings.Daemon.BuildkitDriverOpt,
			})
	}

	return flags
}
