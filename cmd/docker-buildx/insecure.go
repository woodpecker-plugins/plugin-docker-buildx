//go:build insecure
// +build insecure

package main

// allow to alter settings that could the host running the docker socket
const Insecure = true
